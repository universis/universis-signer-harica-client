package org.universis.signer.harica;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HaricaCertificateMessageResult {
    @JsonProperty("Success")
    public Boolean success = true;

    @JsonProperty("Data")
    public HaricaCertificateMessageResultData data;

    @JsonProperty("ErrData")
    public HaricaCertificateErrorResult error;
}
