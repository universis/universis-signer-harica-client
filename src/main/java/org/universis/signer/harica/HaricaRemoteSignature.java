package org.universis.signer.harica;

import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.ExternalSignature;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.Base64;

public class HaricaRemoteSignature implements ExternalSignature {

    private final HaricaSigner _signer;
    private final HaricaSignBufferMessage _message;

    public HaricaRemoteSignature(HaricaSigner signer, String username, String password, String otp) {
        this._signer = signer;
        this._message = new HaricaSignBufferMessage();
        this._message.username = username;
        this._message.password = password;
        this._message.otp = otp;
    }

    @Override
    public String getHashAlgorithm() {
        return "SHA-256";
    }

    @Override
    public String getEncryptionAlgorithm() {
        return "RSA";
    }

    @Override
    public byte[] sign(byte[] bytes) throws GeneralSecurityException {
        HaricaSignBufferResult result;
        // clone message
        HaricaSignBufferMessage newMessage = new HaricaSignBufferMessage();
        newMessage.username = this._message.username;
        newMessage.password = this._message.password;
        newMessage.otp = this._message.otp;
        //
        byte[] hash;
        MessageDigest messageDigest = MessageDigest.getInstance(this.getHashAlgorithm());
        try {
            hash = DigestAlgorithms.digest(new ByteArrayInputStream(bytes), messageDigest);
        } catch (IOException e) {
            throw new GeneralSecurityException(e.getMessage());
        }
        newMessage.buffer = Base64.getEncoder().encodeToString(hash);
        newMessage.flags = Integer.toString(
                HaricaSignBufferFlags.AR_SAPI_SIG_ENFORCE_NO_LOGOUT
                    + HaricaSignBufferFlags.AR_SAPI_SIG_PDF_REVOCATION
                    + HaricaSignBufferFlags.AR_SAPI_SIG_DISABLE_STS
                     + HaricaSignBufferFlags.AR_SAPI_SIG_HASH_ONLY
                     + HaricaSignBufferFlags.AR_SAPI_ENFORCE_SHA1_FLAG
        );
        try {
            result = this._signer.signBuffer(newMessage);
            if (result.success) {
                byte[] sig;
                sig = Base64.getDecoder().decode(result.data.signature);
                return sig;
            }
            throw new GeneralSecurityException(result.error.message);
        } catch (IOException e) {
            throw new GeneralSecurityException(e.getMessage());
        }
    }
}
