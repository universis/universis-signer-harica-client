package org.universis.signer.harica;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.universis.signer.*;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HaricaSignerHandler implements RouterNanoHTTPD.UriResponder {
    private static final Logger log = LogManager.getLogger(KeyStoreHandler.class);
    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {

        SignerAppConfiguration configuration = uriResource.initParameter(SignerAppConfiguration.class);
        if (configuration.keyStore == null) {
            return new ServerErrorHandler("Invalid application configuration. Service keystore cannot be empty at this context").get(uriResource, map, ihttpSession);
        }
        // get authorization header
        String[] usernamePassword;
        log.debug("get authorization header");
        String authorizationHeader = ihttpSession.getHeaders().get("authorization");
        log.debug("validate authorization header");
        if (authorizationHeader != null && authorizationHeader.startsWith("Basic ")) {
            // decode header
            log.debug("decode authorization header");
            byte[] decodedBytes = Base64.decodeBase64(authorizationHeader.replaceFirst("Basic ", ""));
            // and get username and password
            usernamePassword = new String(decodedBytes).split(":");
        } else {
            // otherwise throw forbidden error
            return new ForbiddenHandler().get(uriResource, map, ihttpSession);
        }
        try {
            // parse body
            HashMap<String,String> files = this.parseBody(ihttpSession);
            // get params
            Map<String, List<String>> params = ihttpSession.getParameters();
            // get file to be signed
            String inFileKey = this.getInputFileKey(files);
            // validate file
            if (inFileKey == null) {
                return new BadRequestHandler("Parameter file may not be null.").get(uriResource, map, ihttpSession);
            }
            String inFile = files.get(inFileKey);
            // get file type
            String fileType = "." + FilenameUtils.getExtension(params.get(inFileKey).get(0)).toLowerCase();
            // get file content type
            String fileContentType;
            try {
                fileContentType = this.getContentType(fileType);
            } catch(IOException e) {
                return new BadRequestHandler(e.getMessage()).get(uriResource, map, ihttpSession);
            }
            // convert params to message
            HaricaSignerMessage message;
            try {
                message = this.convertParams(params);
            } catch (Exception ex) {
                return new BadRequestHandler(ex.getMessage()).get(uriResource, map, ihttpSession);
            }
            // add user credentials
            message.username = usernamePassword[0];
            message.password = usernamePassword[1];
            // attach file
            message.attach(inFile);
            // forcibly set file type
            message.fileType = fileType.substring(1);

            if (fileType.equals(".xlsx") || fileType.equals(".docx")) {
                // find signature id
                message.signatureFieldName = HaricaSigner.tryFindSignatureLine(inFile);
            }
            // get timestamp server
            String timestampServer = null;
            if (params.containsKey("timestampServer")) {
                timestampServer = params.get("timestampServer").get(0);
            }
            // get image file
            String imageFile = null;
            for (String key: files.keySet()) {
                if (key.startsWith("image")) {
                    imageFile = files.get(key);
                    break;
                }
            }
            // send message
            HaricaSignerMessageResult result;
            if (fileType.equals(".pdf")) {
                // sign with remote detached signature
                result = new HaricaSigner(configuration.keyStore).sign(message, timestampServer, imageFile);
            } else {
                result = new HaricaSigner(configuration.keyStore).sign(message);
            }
            if (result.success) {
                // set output
                String outFile =  File.createTempFile("signed", fileType).getAbsolutePath();
                byte[] buffer = java.util.Base64.getDecoder().decode(result.data.signedFileData);
                OutputStream outStream = new FileOutputStream(outFile);
                outStream.write(buffer);
                // send response
                NanoHTTPD.Response res = new FileStreamHandler(outFile, fileContentType).get(uriResource, map, ihttpSession);
                CorsHandler.enable(res);
                return res;
            } else {
                return new ServerErrorHandler(new Exception(result.error.message)).get(uriResource, map, ihttpSession);
            }
        } catch(Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
        }
    }

    private String getContentType(String fileType) throws IOException {
        switch (fileType) {
            case ".docx":
                return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            case ".xlsx":
                return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            case ".pdf":
                return "application/pdf";
            default:
                throw new IOException("Unsupported file type.");
        }
    }

    private HashMap<String,String> parseBody(NanoHTTPD.IHTTPSession ihttpSession) throws IOException, NanoHTTPD.ResponseException {
        HashMap<String,String> files = new HashMap<>();
        ihttpSession.parseBody(files);
        for(String ignored : files.values()) {
            // read
        }
        return files;
    }

    private String getInputFileKey(HashMap<String,String> files) {
        for (String key: files.keySet()) {
            if (key.startsWith("file")) {
                return key;
            }
        }
        return null;
    }

    private HaricaSignerMessage convertParams(Map<String, List<String>> params) {
        HaricaSignerMessage result = new HaricaSignerMessage();
        // get page
        result.page = 1;
        if (params.containsKey("page")) {
            result.page = Integer.parseInt(params.get("page").get(0));
        }
        // get dimensions
        if (params.containsKey("position")) {
            String requestPosition = params.get("position").get(0);
            String[] dimensions = requestPosition.split(",");
            if (dimensions.length != 4) {
                throw new InvalidParameterException("Invalid signature position format.");
            }
            for (int i = 0; i < dimensions.length; i++) {
                dimensions[i] = dimensions[i].trim();
            }
            Rectangle position = new Rectangle(Integer.parseInt(dimensions[0]), Integer.parseInt(dimensions[1]), Integer.parseInt(dimensions[2]), Integer.parseInt(dimensions[3]));
            result.setSignaturePosition(position);
        } else {
            // set default position 50, 10, 290, 100
            result.setSignaturePosition(new Rectangle(50, 10, 290, 100));
        }
        // get reason
        if (params.containsKey("reason")) {
            result.reason = params.get("reason").get(0);
        }
        if (params.containsKey("name")) {
            result.signatureFieldName = params.get("name").get(0);
        }
        // get one-time pin
        if (params.containsKey("otp")) {
            result.otp = params.get("otp").get(0);
        } else {
            throw new InvalidParameterException("OTP parameter is missing.");
        }
        return result;
    }

    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }
}
