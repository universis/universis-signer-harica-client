package org.universis.signer.harica;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

public class HaricaSignerMessage {

    @JsonProperty("Username")
    public String username;

    @JsonProperty("Password")
    public String password;

    @JsonProperty("SignPassword")
    public String otp;

    @JsonProperty("Reason")
    public String reason;

    @JsonProperty("Title")
    public String title;

    @JsonProperty("FileType")
    public String fileType;

    @JsonProperty("FileData")
    public String fileData;

    @JsonProperty("Page")
    public Integer page;

    @JsonProperty("Width")
    public Integer width;

    @JsonProperty("Height")
    public Integer height;

    @JsonProperty("X")
    public Integer x;

    @JsonProperty("Y")
    public Integer y;

    @JsonProperty("Appearance")
    public Integer appearance;

    @JsonProperty("GraphicalSignature")
    public String graphicalSignature;

    @JsonProperty("SigFieldName")
    public String signatureFieldName;

    public HaricaSignerMessage() {
    }

    /**
     * Attaches an input stream to this message
     * @param file An input stream which represents the file to attach to this message
     * @param fileType A string which represents the file type of the given input stream e.g. pdf
     * @return Returns this message
     * @throws IOException if the operation fails to access input stream
     */
    HaricaSignerMessage attach(InputStream file, String fileType) throws IOException {
        byte[] bytes = IOUtils.toByteArray(file);
        this.fileData = Base64.getEncoder().encodeToString(bytes);
        this.fileType = fileType;
        return this;
    }

    /**
     * Attaches a file to this message
     * @param fileName A string which represents the file path to attach
     * @return Returns this message
     * @throws IOException if the given file is not accessible
     */
    HaricaSignerMessage attach(String fileName) throws IOException {
        InputStream file = new FileInputStream(fileName);
        String fileType = FilenameUtils.getExtension(fileName);
        return this.attach(file, fileType);
    }

    HaricaSignerMessage setSignaturePosition(Rectangle rect) {
        this.x = rect.x;
        this.y = rect.y;
        this.width = rect.width;
        this.height = rect.height;
        return this;
    }

}
