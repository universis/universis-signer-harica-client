package org.universis.signer.harica;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Base64;

public class HaricaSignerMessageResult {
    @JsonProperty("Success")
    public Boolean success;

    @JsonProperty("Data")
    public HaricaSignerMessageResultData data;

    @JsonProperty("ErrData")
    public HaricaSignerMessageResultError error;

    InputStream getStream() {
        if (this.data == null) {
            throw new NullPointerException("Message data cannot be empty while getting stream.");
        }
        if (this.data.signedFileData == null) {
            throw new NullPointerException("Message file data cannot be empty while getting stream.");
        }
        if (!this.success) {
            throw new IllegalStateException("Message has an invalid state.");
        }
        byte[] bytes = Base64.getDecoder().decode(this.data.signedFileData);
        // return stream
        return new ByteArrayInputStream(bytes);
    }

}
