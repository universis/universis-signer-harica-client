package org.universis.signer.harica;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HaricaSignerMessageResultError {

    @JsonProperty("Message")
    public String message;

    @JsonProperty("InnerCode")
    public int innerCode;

    @JsonProperty("Code")
    public int code;

    @JsonProperty("Module")
    public String module;
}
