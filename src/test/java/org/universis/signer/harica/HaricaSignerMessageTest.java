package org.universis.signer.harica;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.junit.jupiter.api.Assertions.*;
import org.apache.commons.io.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

class HaricaSignerMessageTest {

    @org.junit.jupiter.api.Test
    void shouldSerializeDocument() throws JsonProcessingException {
        HaricaSignerMessage document = new HaricaSignerMessage();
        document.username = "johndoe";
        document.password = "secret";
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String value = mapper.writeValueAsString(document);
        assertNotNull(value);
        assertTrue(value.contains("Username"));
        assertFalse(value.contains("SignPassword"));
        assertFalse(value.contains("Page"));
    }

    @org.junit.jupiter.api.Test
    void shouldDeserializeMessage() throws JsonProcessingException {
        HaricaSignerMessage document = new HaricaSignerMessage();
        document.username = "johndoe";
        document.password = "secret";
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String value = mapper.writeValueAsString(document);
        HaricaSignerMessage res = mapper.readValue(value, HaricaSignerMessage.class);
        assertNotNull(res);
        assertEquals(res.username, "johndoe");
        assertEquals(res.password, "secret");

        document.x = 10;
        document.y = 10;
        document.width = 300;
        document.height = 120;

        assertEquals(10, document.x.intValue());
        assertEquals(10, document.y.intValue());
        assertEquals(300, document.width.intValue());
        assertEquals(120, document.height.intValue());


    }

    @org.junit.jupiter.api.Test
    void shouldSerializeAndDeserializeMessage() throws IOException {
        HaricaSignerMessage document = new HaricaSignerMessage();
        document.username = "johndoe";
        document.password = "secret";
        String filePath = HaricaSignerMessageTest.class.getResource("lorem-ipsum.pdf").getPath();
        InputStream is = new FileInputStream(filePath);
        byte[] bytes = IOUtils.toByteArray(is);
        document.fileData = Base64.getEncoder().encodeToString(bytes);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String value = mapper.writeValueAsString(document);
        assertNotNull(value);
        assertTrue(value.contains("Username"));
        assertTrue(value.contains("FileData"));
        assertFalse(value.contains("SignPassword"));
        assertFalse(value.contains("Page"));
        HaricaSignerMessage res = mapper.readValue(value, HaricaSignerMessage.class);
        assertNotNull(res.fileData, "Expected HaricaSignerDocument.fileData");
    }

    @org.junit.jupiter.api.Test
    void shouldAttachDocument() throws IOException {
        HaricaSignerMessage document = new HaricaSignerMessage();
        document.username = "johndoe";
        document.password = "secret";
        String filePath = HaricaSignerMessageTest.class.getResource("lorem-ipsum.pdf").getPath();
        HaricaSignerMessage thisInstance = document.attach(filePath);
        assertEquals(thisInstance.getClass(), HaricaSignerMessage.class);
        assertNotNull(document.fileData);
        assertNotNull(document.fileType);
        assertEquals(document.fileType, "pdf");

        document = new HaricaSignerMessage();
        document.username = "johndoe";
        document.password = "secret";
        InputStream is = new FileInputStream(filePath);
        thisInstance = document.attach(is, "pdf");
        assertEquals(thisInstance.getClass(), HaricaSignerMessage.class);
        assertNotNull(document.fileData);
        assertNotNull(document.fileType);
        assertEquals(document.fileType, "pdf");


    }

}